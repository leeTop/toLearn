package lee.detail.TbkTpwdCreate;

import com.taobao.api.ApiRuleException;
import com.taobao.api.BaseTaobaoRequest;
import com.taobao.api.internal.util.RequestCheckUtils;
import com.taobao.api.internal.util.TaobaoHashMap;

import java.util.Map;

/**
 * 描述:
 *
 * @author Leo
 * @create 2018-01-12 上午 12:42
 */
public class TbkTpwdCreateRequest extends BaseTaobaoRequest<TbkTpwdCreateResponse> {
    /**
     * 生成口令的淘宝用户ID
     */
    private String userId;
    /**
     * 口令弹框内容
     */
    private String text;
    /**
     * 口令跳转目标页
     */
    private String url;
    /**
     * 口令弹框logoURL
     */
    private String logo;
    /**
     * 扩展字段JSON格式
     */
    private String ext;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    @Override
    public String getApiMethodName() {
        return "taobao.tbk.tpwd.create";
    }

    @Override
    public Map<String, String> getTextParams() {
        TaobaoHashMap txtParams = new TaobaoHashMap();
        txtParams.put("user_id",userId);
        txtParams.put("text",text);
        txtParams.put("url",url);
        txtParams.put("logo",logo);
        txtParams.put("ext",ext);
        if(this.udfParams != null) {
            txtParams.putAll(this.udfParams);
        }
        return txtParams;
    }

    @Override
    public Class<TbkTpwdCreateResponse> getResponseClass() {
        return TbkTpwdCreateResponse.class;
    }

    @Override
    public void check() throws ApiRuleException {
        RequestCheckUtils.checkNotEmpty(text,"text");
        RequestCheckUtils.checkNotEmpty(url,"url");
    }

}
