package lee.detail.TbkTpwdCreate;

import com.taobao.api.TaobaoResponse;
import com.taobao.api.internal.mapping.ApiField;

/**
 * 描述:
 *
 * @author Leo
 * @create 2018-01-12 上午 12:43
 */
public class TbkTpwdCreateResponse extends TaobaoResponse {

    private static final long serialVersionUID = 5379783561624533287L;
    @ApiField("data")
    private ReqData data;

    public ReqData getData() {
        return data;
    }

    public void setData(ReqData data) {
        this.data = data;
    }
}
