package lee.detail.TbkTpwdCreate;

import com.taobao.api.TaobaoObject;
import com.taobao.api.internal.mapping.ApiField;

/**
 * 描述:
 *
 * @author Leo
 * @create 2018-01-12 上午 1:00
 */
public class ReqData extends TaobaoObject {
    /**
     * password
     */
    @ApiField("model")
    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
