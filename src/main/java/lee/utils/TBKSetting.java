package lee.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 描述:
 * 淘宝客账户设置
 *
 * @author Leo
 * @create 2018-01-11 下午 9:34
 */
@Component
@ConfigurationProperties(prefix="tbk")
public class TBKSetting {
    /**
     * 淘宝客userId
     */
    private String userId;
    /**
     * 淘宝客appkey
     */
    private String appkey;
    /**
     * 淘宝客appSecret
     */
    private String appSecret;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }


}
