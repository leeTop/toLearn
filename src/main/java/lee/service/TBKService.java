package lee.service;

import lee.detail.TbkTpwdCreate.TbkTpwdCreateResponse;

/**
 * 描述:
 * 淘宝客服务类
 *
 * @author Leo
 * @create 2018-01-11 下午 9:56
 */
public interface TBKService {
    public TbkTpwdCreateResponse twpd ();
}
