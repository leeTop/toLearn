package lee.service.impl;

import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import lee.detail.TbkTpwdCreate.TbkTpwdCreateRequest;
import lee.detail.TbkTpwdCreate.TbkTpwdCreateResponse;
import lee.service.TBKService;
import lee.utils.TBKSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 描述:
 * 淘宝客服务实现类 由于网站内容简单 无法获取高等API权限,到此为止
 *
 * @author Leo
 * @create 2018-01-11 下午 9:57
 */
@Service
public class TBKServiceImpl implements TBKService {
    @Autowired
    private TBKSetting tbkSetting;

    @Override
    public TbkTpwdCreateResponse twpd() {
        String url = "http://gw.api.taobao.com/router/rest";
        TaobaoClient client = new DefaultTaobaoClient(url, tbkSetting.getAppkey(), tbkSetting.getAppSecret(), "json");
        TbkTpwdCreateRequest req = new TbkTpwdCreateRequest();
        req.setUserId(tbkSetting.getUserId());
        req.setText("复制淘口令粘贴到淘宝");
        req.setUrl("https://uland.taobao.com/");
        req.setLogo("https://uland.taobao.com/");
        req.setExt("{}");
        TbkTpwdCreateResponse rsp = new TbkTpwdCreateResponse();
        try {
            rsp = client.execute(req);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return rsp;
    }
}
