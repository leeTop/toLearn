package lee.repository;

import lee.domain.Movie;

/**
 * 描述:
 * 电影仓库类
 *
 * @author Leo
 * @create 2017-12-26 下午 7:13
 */
public interface MovieRepository extends Repository<Movie> {

}
